﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTool : Tool
{
	public float damage = 0f;
	public float attackSpeed = 0f;

	public string type = "";                // "Melee" or "Ranged"
}
