﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ZoomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	[SerializeField]
	private CameraScript playerCamera;

	[SerializeField]
	private bool isZoomIn;

	private bool isPressed;

	private void FixedUpdate()
	{
		if (isPressed)
		{
			if (isZoomIn)
			{
				playerCamera.ZoomIn();
			}
			else
			{
				playerCamera.ZoomOut();
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		isPressed = true;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		isPressed = false;
	}
}
